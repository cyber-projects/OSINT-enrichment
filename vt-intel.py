import json
import urllib
import time
import re
#import itertools
from pprint import pprint

#declare global variables
url = 'https://www.virustotal.com/vtapi/v2/ip-address/report'
apiKey = 'insert VT API Key'

#functions

#get intel from virustotal
def get_intel(IP, apiKey):
	parameters = {'ip': IP, 'apikey': apiKey}
	response = urllib.urlopen('%s?%s' % (url, urllib.urlencode(parameters))).read()
	response_dict = json.loads(response)
	#pprint (response_dict)
	return (response_dict)

IPs_input=raw_input("Enter IPs separated by ',': ")
IPs = IPs_input.split(',')
for IP in IPs:
	vtIntel = get_intel(IP, apiKey)
	domains=vtIntel['detected_urls']
	#pprint (domains)
	dates=[]
	for item in domains:
		dates.append((item['scan_date'])[:7])


	dates=(list(set(dates)))
	for date in dates:
		firstTime= str(int(date[5:7])) + "/1/" + date[0:4] + ":00:00:00"
		lastTime=str((int(date[5:7])+1)) + "/1/" + date[0:4] + ":00:00:00"
		splunk_query = "index=index sourcetype=proxy " + firstTime + " " + lastTime
		for item in domains:
			if date == (item['scan_date'][:7]):
				splunk_query+=" domain=" + re.search("^([\w\d\.-]*[\w]*)" ,str(item['url'][7:])).group(0) + " "
		print splunk_query
